package examples;

import java.util.Vector;

import file_operations.Serialize;
import phisics_engine.Particle;
import phisics_engine.SimulationData;
import phisics_engine.Vector3D;
import phisics_engine.fields.Magneticfield;

public class Examples {

	static void particle_in_field() {
		//Antiproton in Magnetic Field
		Vector3D v = new Vector3D(1, 0, 0);
		Vector3D r = new Vector3D();
		
		Magneticfield B = new Magneticfield(new Vector3D(0, 0, 1));
		Particle a = new Particle(-1,1,v,r);
		Vector<Particle> czastki = new Vector<Particle>();
		czastki.add(a);
		SimulationData data = new SimulationData(czastki, B);
		Serialize.save_simulation(data, "examples/antiproton_in_magnetic_field.sav");
	}
	static void two_protons() {
		//Two protons first standing still
		Vector3D r1 = new Vector3D(-10, 0, 0);
		Vector3D r2 = new Vector3D(10, 0, 0);
		Particle a = new Particle(1,1,new Vector3D(),r1);
		Particle b = new Particle(1,1,new Vector3D(),r2);
		Vector<Particle> czastki = new Vector<Particle>();
		czastki.add(a);
		czastki.add(b);
		
		SimulationData data = new SimulationData(czastki);
		Serialize.save_simulation(data, "examples/two_protons.sav");
	}
	static void three_protons() {
		//Two protons first standing still
		Vector3D r1 = new Vector3D(-10, 0, 0);
		Vector3D r2 = new Vector3D(10, 0, 0);
		Vector3D r3 = new Vector3D(0, 10 * Math.sqrt(3), 0);
		Particle a = new Particle(1,1,new Vector3D(),r1);
		Particle b = new Particle(1,1,new Vector3D(),r2);
		Particle c = new Particle(1,1,new Vector3D(),r3);
		Vector<Particle> czastki = new Vector<Particle>();
		czastki.add(a);
		czastki.add(b);
		czastki.add(c);
		
		SimulationData data = new SimulationData(czastki);
		Serialize.save_simulation(data, "examples/three_protons.sav");
	}
	static void proton_and_negative_mass_proton() {
		//Two protons first standing still
		Vector3D r1 = new Vector3D(-10, 0, 0);
		Vector3D r2 = new Vector3D(10, 0, 0);
		Particle a = new Particle(1,1,new Vector3D(),r1);
		Particle b = new Particle(-1,-1,new Vector3D(),r2);
		Vector<Particle> czastki = new Vector<Particle>();
		czastki.add(a);
		czastki.add(b);
		
		SimulationData data = new SimulationData(czastki);
		Serialize.save_simulation(data, "examples/proton_and_negative_mass_proton.sav");
	}
	static void blank() {
		//Blank workspace
		Vector<Particle> czastki = new Vector<Particle>();
		SimulationData data = new SimulationData(czastki);
		Serialize.save_simulation(data, "examples/blank.sav");
	}
	
	public static void main(String[] args) {

		particle_in_field();
		two_protons();
		three_protons();
		proton_and_negative_mass_proton();
		blank();

	}

}
