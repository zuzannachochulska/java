package file_operations;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import phisics_engine.SimulationData;

public class Serialize {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void save_simulation(SimulationData simulation, String a) {
		try {
	         FileOutputStream fileOut = new FileOutputStream(a);
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(simulation);
	         out.close();
	         fileOut.close();
	         System.out.println("Serialized data is saved in " + a);
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
}
