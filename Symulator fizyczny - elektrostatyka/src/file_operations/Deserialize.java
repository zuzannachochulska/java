package file_operations;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import phisics_engine.SimulationData;

public class Deserialize {
	public static SimulationData load_simulation(String a) {
		SimulationData load_data = null;
		try {
	         FileInputStream fileIn = new FileInputStream(a);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         load_data = (SimulationData) in.readObject();
	         in.close();
	         fileIn.close();
	         System.out.println("Udało się!");
	      } catch (IOException i) {
	         i.printStackTrace();
	      } catch (ClassNotFoundException c) {
	         System.out.println("Save has been corrupted!");
	         c.printStackTrace();
	      }
		return load_data;
	}
}
