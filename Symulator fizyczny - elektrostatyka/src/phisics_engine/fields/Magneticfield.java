package phisics_engine.fields;

import phisics_engine.Particle;
import phisics_engine.Vector3D;

public class Magneticfield implements Field{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Magneticfield(Vector3D magnetic_field) {
		B = new Vector3D(magnetic_field);
	}

	@Override
	public Vector3D particle_field_interaction(Particle a) {
		
		Vector3D intermediate = Vector3D.vector_product(a.get_velocity(), B);
		
		return Vector3D.multiply_by_scalar(intermediate, a.get_charge());
	}
	
	public Vector3D B; 
}
