package phisics_engine.fields;

import java.io.Serializable;

import phisics_engine.Particle;
import phisics_engine.Vector3D;

public interface Field extends Serializable{
	Vector3D particle_field_interaction(Particle a);
}
