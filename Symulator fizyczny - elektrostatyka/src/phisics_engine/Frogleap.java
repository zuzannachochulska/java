package phisics_engine;

import java.util.Vector;

import phisics_engine.fields.Field;


public class Frogleap{
	
	
	public static void acceleration_calculator(Vector<Particle> particles){

		Vector3D[][] force_matrix = force_matrix_calculator(particles);
		//Adding forces
		Vector3D[] force_table = new Vector3D[particles.size()];
		
		
		//Calculating acceleration
		for (int i = 0; i<particles.size();i++) {
			force_table[i] = new Vector3D();
			for (int j = 0;j<particles.size();j++) {
				force_table[i].add(force_matrix[i][j]);
			}
			particles.get(i).set_a(Particle.particles_acceleration(force_table[i], particles.get(i).get_mass()));
		}	
	}
	//made for new Simulationdata class
	public static void acceleration_calculator(SimulationData data){
		//calculating force of particle interactions
		acceleration_calculator(data.get_particles());
		
		//Calculating force of field interactions
		for(Particle part : data.get_particles()) {
			Vector3D field_force = new Vector3D();
			//For all fields
			for(Field fiel : data.get_fields()) {
				
				//Calculate force of interaction between aprticle and field
				field_force.add(fiel.particle_field_interaction(part));
			}
			//Calculate acceleration
			part.add_a(Vector3D.divide_by_scalar(field_force, part.get_mass()));
		}
	}
	
	
	
	
	//Calculating all particle interactions, creating a neat matrix
	private static Vector3D[][] force_matrix_calculator (Vector<Particle> local_particles_vector){

		Vector3D[][] local_force_matrix = new Vector3D[local_particles_vector.size()][local_particles_vector.size()];
		
		
		//Sets zeros on diagonal
		for (int i = 0; i<local_particles_vector.size();i++)local_force_matrix[i][i] = new Vector3D ();
		
		//Rest of the matrix
		for (int i = 0; i<local_particles_vector.size();i++)
			for (int j = i+1;j<local_particles_vector.size();j++) {

				Vector3D force_of_two_particles = Particle.electrostatic_force(local_particles_vector.get(i),local_particles_vector.get(j));
				
				local_force_matrix[i][j] = force_of_two_particles;
				local_force_matrix[j][i] = Vector3D.oposite_vector(force_of_two_particles);
				
			}
		return local_force_matrix;
	}
	
	
}