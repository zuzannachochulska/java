package phisics_engine;
import java.io.Serializable;

public class Vector3D implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Constructors
	public Vector3D() {
		this.x=0;
		this.y=0;
		this.z=0;
	}
	public Vector3D(double a, double b, double c) {
		this.x=a;
		this.y=b;
		this.z=c;
	}
	public Vector3D(double lenght, Vector3D direction) {
		this.x=lenght*direction.x;
		this.y=lenght*direction.y;
		this.z=lenght*direction.z;
	}
	public Vector3D(Vector3D a) {
		this.x=a.x;
		this.y=a.y;
		this.z=a.z;
	}
	//Set_zero method
	public void set_zero() {
		this.x=0;
		this.y=0;
		this.z=0;
	}
	//Basic vector math
	public static Vector3D plus(Vector3D a, Vector3D b){
		//a+b
		return new Vector3D(a.x+b.x,a.y+b.y,a.z+b.z);
	}
	public static Vector3D minus(Vector3D a, Vector3D b){
		//a-b
		return new Vector3D(a.x-b.x,a.y-b.y,a.z-b.z);
	}
	public static Vector3D divide_by_scalar(Vector3D a, double b){
		//a/b
		return new Vector3D(a.x/b,a.y/b,a.z/b);
	}
	public static Vector3D multiply_by_scalar(Vector3D a, double b){
		//a*b
		return new Vector3D(a.x*b,a.y*b,a.z*b);
	}
	public static double scalar_product(Vector3D a, Vector3D b){
		//a*b scalar product
		return a.x*b.x+a.y*b.y+a.z*b.z;
	}
	public static Vector3D vector_product(Vector3D a, Vector3D b){
		//axb vector product
		return new Vector3D(a.y*b.z-a.z*b.y,-a.x*b.z+b.x*a.z,a.x*b.y-b.x*a.y);
	}
	public static double lenght_squared(Vector3D particle_a, Vector3D particle_b){
		//x^2+y^2+z^2
		return Math.pow(particle_a.x-particle_b.x,2)+Math.pow(particle_a.y-particle_b.y,2)+Math.pow(particle_a.z-particle_b.z,2);
	}
	
	public static Vector3D directional_vector(Vector3D a, Vector3D b){
		//vec_r/r
		return divide_by_scalar((minus(a,b)),Math.sqrt(lenght_squared(a,b)));
	}
	public static Vector3D oposite_vector(Vector3D a) {
		return new Vector3D (-a.get_x(), -a.get_y(),-a.get_z());
	}
	//Getters, adders
	public double get_x() {
		return x;
	}
	public double get_y() {
		return y;
	}
	public double get_z() {
		return z;
	}
	public String get_vector() {
		return get_x() + " " + get_y() + " " + get_z();
	}
	//Adder
	public void add(Vector3D a) {
		this.x+=a.x;
		this.y+=a.y;
		this.z+=a.z;
	}
	private double x,y,z;
}