package phisics_engine;

import java.io.Serializable;

public class Particle implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//public final static double k =  8.987551792e9 * 1.602176634e-19 * 1.602176634e-19;
	public final static double k =  100;
	//Constructors
	public Particle()
	{
		charge = 0;
		this.mass = 1;
		this.velocity = new Vector3D();
		this.position =  new Vector3D();
		this.acceleration =  new Vector3D();
	}
	public Particle(int charge, double mass){
		this.charge = charge;
		if(mass == 0.0)
			throw new IllegalArgumentException("particle mass must be non-zero");
		this.mass = mass;
		this.velocity = new Vector3D();
		this.position =  new Vector3D();
		this.acceleration =  new Vector3D();
	}
	public Particle(int charge, double mass, Vector3D velocity, Vector3D position){
		this.charge = charge;
		if(mass == 0.0)
			throw new IllegalArgumentException("mass must be non-zero");
		this.mass = mass;
		this.velocity = new Vector3D(velocity);
		this.position = new Vector3D(position);
		this.acceleration =  new Vector3D();
	}
	//Basic electrostatic interaction
	public static Vector3D electrostatic_force(Particle particle_a, Particle particle_b) {
		
		double l = Vector3D.lenght_squared(particle_a.get_position(), particle_b.get_position());
		
		if(l == 0.0)return new Vector3D();
		
		//Scalar part of the equation
		double force_length=k*particle_a.get_charge()*particle_b.get_charge()/l;
		//Returning force vector
		return new Vector3D(force_length,Vector3D.directional_vector(particle_a.get_position(), particle_b.get_position()));
	}
	//Newtons Second Law of Motion
	public static Vector3D particles_acceleration(Vector3D force,double d) {
		return Vector3D.divide_by_scalar(force,d);
	}
	//Getters
	public int get_charge(){
		return this.charge;
	}
	public double get_mass(){
		return this.mass;
	}
	public Vector3D get_position(){
		return this.position;
	}
	public Vector3D get_acceleration(){
		return this.acceleration;
	}
	public Vector3D get_velocity(){
		return this.velocity;
	}
	//Setters
	public void set_r(Vector3D a) {
		this.position = a;
	}
	public void set_v(Vector3D a) {
		this.velocity = a;
	}
	public void set_a(Vector3D a) {
		this.acceleration = a;
	}
	//Adders
	public void add_r(Vector3D a) {
		this.position.add(a);
	}
	public void add_v(Vector3D a) {
		this.velocity.add(a);
	}
	public void add_a(Vector3D a) {
		this.acceleration.add(a);
	}
	private int charge;
	private Vector3D  position, velocity, acceleration;
	private double mass;
}