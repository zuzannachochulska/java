package user_interface;

import java.util.Vector;

public class QandA {
	
	private String question, answer;
	public QandA (String q, String a) {
		answer = a;
		question = q;
	}
	public static Vector<QandA> generate(){
		Vector<QandA> qa = new Vector<QandA>();
		qa.add(new QandA("Cokolwiek","Odpowiedź 1"));
		qa.add(new QandA("Pytanie 2","Odpowiedź 2"));
		qa.add(new QandA("Pytanie 3","Odpowiedź 3"));
		qa.add(new QandA("Pytanie 4","Odpowiedź 4"));
		qa.add(new QandA("Pytanie 5","Odpowiedź 5"));
		qa.add(new QandA("Pytanie 6","Odpowiedź 6"));
		qa.add(new QandA("Pytanie 7","Odpowiedź 7"));
		return qa;
	}
	
	public String get_question() {
		return question;
	}
	public String get_answer() {
		return answer;
	}
	
}
