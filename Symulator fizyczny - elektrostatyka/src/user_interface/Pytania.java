package user_interface;

import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;


@SuppressWarnings("serial")
public class Pytania extends JFrame {
	 JLabel wyswietlaczodpowiedzi;
	 public Pytania () {
		 this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		 this.setSize(1000,600); 
		 this.setTitle("Najczęściej zadawane pytania");
	 
		 Vector <QandA> qa_vector = QandA.generate();
		 //Generates vector of QandA's
		 //To get question use qa_vector.get(i).get_question()
		 //To get answer use qa_vector.get(i).get_answer()
		 
		 
		DefaultListModel listaElementy = new DefaultListModel();
		JList lista = new JList(listaElementy);
		
		
		ListSelectionListener listaListener = new ListSelectionListener() {
			
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting()) return;
				for (Object wybrane : lista.getSelectedValuesList()) {
					wyswietlaczodpowiedzi.setText((String) wybrane);
				}
			}
		};
		
		
		
		
		
				
			//setSize(400,400);
			setDefaultCloseOperation(EXIT_ON_CLOSE);
				
			for (int i=0; i < qa_vector.size(); i++)
				listaElementy.addElement(qa_vector.get(i));
			
			lista.addListSelectionListener(listaListener);
			

			 lista.setVisibleRowCount(10);
		     
			 //Dodawanie JScrollPane:
			 JScrollPane listScrollPane = new JScrollPane(lista);
			 listScrollPane.setPreferredSize( new Dimension(500,100));
		     
		     JPanel panelPrzyciskow = new JPanel (new FlowLayout(FlowLayout.LEFT));
		     
		     wyswietlaczodpowiedzi = new JLabel("Wybierz pytanie");
		     
		     ActionListener wyswietlanieodpowiedzi = new ActionListener() {
		    	 @Override
					public void actionPerformed(ActionEvent e) {
						for (Object wybrane : lista.getSelectedValuesList())
							listaElementy.removeElement(wybrane);
						
					}
		     };
		     
		     
		     
		     panelPrzyciskow.add(wyswietlaczodpowiedzi);
		     
		 	
		     //add(lista, BorderLayout.CENTER);
		     add(listScrollPane, BorderLayout.CENTER);
		     add(panelPrzyciskow, BorderLayout.SOUTH);
		     
		     pack();		
		
		
	}
	 
	 public static void main(String[] args) {
			Pytania ramka = new Pytania();
			ramka.setVisible(true);
			
			
		}
}

